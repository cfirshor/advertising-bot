import asyncio

from aiogram import executor

from bot.handlers import *
from bot.models import init_db, Ad
from bot.tasks import send_repeatedly


async def start_tasks(*args, **kwargs):
    ads = await Ad.filter(enabled=True)
    for ad in ads:
        task_name = 'task_{}'.format(ad.id)
        asyncio.create_task(send_repeatedly(ad), name=str(task_name))


if __name__ == '__main__':
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(init_db())
    executor.start_polling(dispatcher, skip_updates=True, on_startup=start_tasks)
    for task in asyncio.all_tasks():
        task.cancel()
