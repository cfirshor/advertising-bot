import asyncio
import datetime
import json
from pprint import pprint

from bot.models import init_db, User, Channel, Ad, AdMedia, AdLink


DUMP_FILEPATH = 'dump.json'


async def load_db():
    with open(DUMP_FILEPATH) as f:
        dump = json.load(f)
    await init_db()
    mapping = [
        ('User', User), ('Channel', Channel), ('Ad', Ad), ('AdMedia', AdMedia), ('AdLink', AdLink)
    ]
    for model_name, model in mapping:
        data = dump[model_name]
        for item in data:
            if model_name == 'Ad':
                # item['updated_at'] = datetime.datetime.fromisoformat(item['updated_at'])
                del item['updated_at']
                item['cron_expr'] = '{} * * *'.format(item['interval'])
                channels = item.pop('channels')
            obj = await model.create(**item)
            if model_name == 'Ad':
                channels = await Channel.filter(id__in=channels)
                await obj.channels.add(*channels)
    print('Loaded from {}'.format(DUMP_FILEPATH))


if __name__ == '__main__':
    asyncio.run(load_db())
