import asyncio
import json
from pprint import pprint

from bot.models import init_db, User, Channel, Ad, AdMedia, AdLink


DUMP_FILEPATH = 'dump.json'


async def dump_db():
    await init_db()
    dump = {
        'User': [dict(user) for user in await User.all()],
        'Channel': [dict(channel) for channel in await Channel.all()],
        'AdMedia': [dict(media) for media in await AdMedia.all()],
        'AdLink': [dict(link) for link in await AdLink.all()],
    }
    ads = await Ad.all()
    ads_json = []
    for ad in ads:
        data = dict(ad)
        data['updated_at'] = ad.updated_at.isoformat()
        data['channels'] = [channel.id for channel in await ad.channels.all()]
        ads_json.append(data)
    dump['Ad'] = ads_json
    with open(DUMP_FILEPATH, 'w') as f:
        json.dump(dump, f, indent=4)
    print('Dumped to {}'.format(DUMP_FILEPATH))


if __name__ == '__main__':
    asyncio.run(dump_db())
