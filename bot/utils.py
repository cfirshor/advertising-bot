import asyncio
from aiogram.utils.exceptions import RetryAfter
import datetime

from aiogram.types import ParseMode
from aiogram.types.input_media import MediaGroup
from markdown import markdown

from . import keyboards
from .bot import bot


async def stop_task(task_name):
    for task in asyncio.all_tasks():
        if task.get_name() == task_name:
            task.cancel()
            break


async def send_ad(ad, chat_id):
    text = ad.text or ''
    links = await ad.links.all()
    media = await ad.media.all()
    now = datetime.datetime.now()

    if len(media) > 1:
        media_group = []
        for item in media:
            item = {'media': item.file_id, 'type': item.type}
            media_group.append(item)
        links = ['<a href="{}"><b>{}</b></a>'.format(link.url, link.text) for link in links]
        text += '\n\n' + '\n'.join(links)
        media_group[0]['caption'] = text
        try:
            messages = await bot.send_media_group(chat_id, media_group)
        except RetryAfter as ex:
            warning_c = '\033[93m'
            end_c = '\033[0m'
            print(f'{warning_c}Timeout Utils:{end_c}')
            print(' {} Will be sent in: {} seconds.\n Current time: {}'.format(ad.name, ex.timeout, now))
            await asyncio.sleep(delay=ex.timeout)
            messages = await bot.send_media_group(chat_id, media_group)
        return [item.message_id for item in messages]

    reply_markup = keyboards.ad_links(links)
    if media:
        file = media[0]
        method = getattr(bot, 'send_{}'.format(file.type))
        try:
            message = await method(chat_id, file.file_id, caption=text, reply_markup=reply_markup)
        except RetryAfter as ex:
            warning_c = '\033[93m'
            end_c = '\033[0m'
            print(f'{warning_c}Timeout Utils(media):{end_c}')
            print(' {} Will be sent in: {} seconds.\n Current time: {}'.format(ad.name, ex.timeout, now))
            await asyncio.sleep(delay=ex.timeout)
            message = await method(chat_id, file.file_id, caption=text, reply_markup=reply_markup)
    else:
        message = await bot.send_message(chat_id, text, reply_markup=reply_markup)
    return [message.message_id]


# def 