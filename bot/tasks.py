import asyncio
import datetime
import math

# from aiogram import Bot
from aiogram.utils.exceptions import BotKicked, BadRequest, RetryAfter
from croniter import croniter

# from .bot import dispatcher
from .utils import send_ad


async def send_repeatedly(ad):
    iter1 = croniter(ad.cron_expr, start_time=datetime.datetime.now())
    while True:
        now = datetime.datetime.now()
        post_at = iter1.get_next(datetime.datetime)
        delay = post_at - now
        delay = delay.total_seconds()
        print('Cron ({}):'.format(ad.name), now, post_at, delay)
        await asyncio.sleep(delay)
        await ad.refresh_from_db()
        for channel in await ad.channels.all():
            try:
                await send_ad(ad, channel.chat_id)
            except (BotKicked, BadRequest):
                # TODO: send notify
                pass


    # now = datetime.datetime.now()
    # if now > send_at:
    #     diff = now - send_at
    #     diff = math.ceil(diff.total_seconds() / interval) * interval
    #     send_at += datetime.timedelta(seconds=diff)
    # while True:
    #     delay = send_at - now
    #     await asyncio.sleep(delay.total_seconds())
    #     channels = await ad.channels.all()
    #     for obj in channels:
    #         try:
    #             await ad.send(bot, obj.chat_id)
    #         except BotKicked:
    #             pass
    #     send_at += datetime.timedelta(seconds=interval)
    #     ad.send_at = send_at
    #     await ad.save()
    #     now = datetime.datetime.now()


# async def send_ads():
#     ads = await AdsState.filter(enabled=True)
#     for ad in ads:
#         asyncio.create_task(send_repeatedly(ad))
