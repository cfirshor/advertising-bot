import asyncio
import enum
import functools

from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types.reply_keyboard import KeyboardButton, ReplyKeyboardMarkup


PAGE_SIZE = 30


def inline_markup(func):

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        keyboard = []
        for row in func(*args, **kwargs):
            keyboard.append([InlineKeyboardButton(**data) for data in row])
        return InlineKeyboardMarkup(inline_keyboard=keyboard)

    return wrapper


def inline_markup_async(func):

    async def wrapper(*args, **kwargs):
        keyboard = []
        for row in await func(*args, **kwargs):
            keyboard.append([InlineKeyboardButton(**data) for data in row])
        return InlineKeyboardMarkup(inline_keyboard=keyboard)

    return wrapper


@inline_markup
def main_menu():
    return [
        [{'text': '🎯 Channels', 'callback_data': 'channels'}],
        [{'text': '🎙 Ads', 'callback_data': 'ads'}],
        # [{'text': '⚙ Settings', 'callback_data': 'settings'}],
    ]


def paginate_items(items, page=1):
    paginate = len(items) > PAGE_SIZE
    if paginate:
        start_i = (page - 1) * PAGE_SIZE
        end_i = page * PAGE_SIZE
        items = items[start_i:end_i]
    return items, paginate


def pagination(page):
    return [
        {'text': '⏪ Prev', 'callback_data': 'prev'},
        {'text': 'Page {}'.format(page), 'callback_data': 'blank'},
        {'text': 'Next ⏩', 'callback_data': 'next'}
    ]


@inline_markup
def channels_list(channels, page=1):
    result = []
    channels, paginated = paginate_items(channels, page)
    for item in channels:
        result.append([{'text': item.name, 'callback_data': str(item.id)}])
    if paginated:
        result.append(pagination(page))
    result.append([{'text': '↩ Back', 'callback_data': 'to_menu'}])
    return result


@inline_markup
def channel_detail(channel):
    rows = [
        [{'text': '🗑 Remove', 'callback_data': 'remove'}],
        [{'text': '↩ Back', 'callback_data': 'back'}]
    ]
    url = channel.url
    if url:
        rows.insert(0, [{'text': '⤴ Open', 'url': url}])
    return rows


@inline_markup
def channels_select_multiple(channels, selected=[], page=1):
    result = []
    channels, paginated = paginate_items(channels, page)
    for item in channels:
        item_id = str(item.id)
        icon = '☑' if item_id in selected else '◻'
        result.append([{'text': '{} {}'.format(icon, item.name), 'callback_data': item_id}])
    if result:
        result.append([{'text': '✅ All', 'callback_data': 'all'}])
        result.append([{'text': '💾 Save', 'callback_data': 'save'}])
    if paginated:
        result.append(pagination(page))
    result.append([{'text': '↩ Back', 'callback_data': 'back'}])
    return result


@inline_markup
def ads_menu():
    return [
        [{'text': '➕ Create', 'callback_data': 'create'}],
        [{'text': '🗂 My ads', 'callback_data': 'my_ads'}],
        [{'text': '↩ Back', 'callback_data': 'to_menu'}]
    ]


@inline_markup_async
async def ads_list(ads, page=1):
    result = []
    channels, paginated = paginate_items(ads, page)
    for item in channels:
        emojis = []
        if item.text:
            emojis.append('💬')
        file_types = await item.media.all().group_by('type').values_list('type', flat=True)
        for type, emoji in [('photo', '📷'), ('video', '🎞'), ('animation', '🎞')]:
            if type in file_types:
                emojis.append(emoji)
        if await item.links.all().exists():
            emojis.append('🔗')
        enabled = '🔈' if item.enabled else '🔇'
        emojis = ' '.join(emojis)
        text = '{} ┃ {} ┃ {} ┃ {}'.format(emojis, item.name, item.cron_expr, enabled)
        result.append([{'text': text, 'callback_data': str(item.id)}])
    if paginated:
        result.append(pagination(page))
    result.append([{'text': '↩  Back', 'callback_data': 'back'}])
    return result


@inline_markup
def ad_detail(ad):
    status = '🔈 Enabled' if ad.enabled else '🔇 Disabled'
    return [
        [{'text': '✏ Rename', 'callback_data': 'rename'}],
        [{'text': '💬 Text', 'callback_data': 'text'}],
        [{'text': '📷 Media', 'callback_data': 'media'}],
        [{'text': '🔗 Link buttons', 'callback_data': 'links'}],
        [{'text': '⏳ {}'.format(ad.cron_expr), 'callback_data': 'schedule'}],
        [{'text': '🎯 Channels', 'callback_data': 'channels'}],
        [{'text': status, 'callback_data': 'status'}],
        [{'text': '🗑 Delete', 'callback_data': 'delete'}],
        [{'text': '↩ Back', 'callback_data': 'back'}]
    ]


@inline_markup
def link_buttons():
    return [
        [{'text': '➕ Add new', 'callback_data': 'new'}],
        [{'text': '🎹 Current buttons', 'callback_data': 'list'}],
        [{'text': '↩ Back', 'callback_data': 'back'}]
    ]


@inline_markup
def links_list(links):
    res = []
    for link in links:
        row = [
            {'text': link.text, 'url': link.url},
            {'text': '⬆', 'callback_data': 'up_{}'.format(link.id)},
            {'text': '⬇', 'callback_data': 'down_{}'.format(link.id)},
            {'text': '🗑', 'callback_data': 'delete_{}'.format(link.id)}
        ]
        res.append(row)
    res.append([{'text': '↩ Back', 'callback_data': 'back'}])
    return res


@inline_markup
def ad_links(links):
    return [[{'text': link.text, 'url': link.url}] for link in links]


@inline_markup
def ad_set_time():
    return [
        [{'text': '▶ Start now', 'callback_data': 'now'}],
        [{'text': '↩ Back', 'callback_data': 'back'}]
    ]


@inline_markup
def settings():
    return [
        [{'text': '⏳ Global Interval', 'callback_data': 'interval'}],
        [{'text': '↩ Back', 'callback_data': 'back'}]
    ]


@inline_markup
def back(to_menu=True):
    callback = 'to_menu' if to_menu else 'back'
    return [
        [{'text': '↩ Back', 'callback_data': callback}]
    ]


@inline_markup
def yes_no():
    return [
        [{'text': '✅ Yes', 'callback_data': 'yes'}, {'text': '❌ No', 'callback_data': 'no'}]
    ]


@inline_markup
def confirm():
    return [
        [{'text': '✅ Confirm', 'callback_data': 'correct'}],
        [{'text': '↩ Back', 'callback_data': 'back'}]   
    ]


def done_reply_keyboard():
    keyboard = [
        [KeyboardButton(text='✔ Done')],
        [KeyboardButton(text='❌ Cancel')]
    ]
    return ReplyKeyboardMarkup(keyboard, resize_keyboard=True, one_time_keyboard=True)


# @inline_markup
# def navigate():
#     return [
#         [{'text': '↩ Back', 'callback_data': 'back'}],
#         [{'text': '❌ Cancel', 'callback_data': 'cancel'}]
#     ]
