from aiogram.dispatcher.filters.state import State, StatesGroup


class MainState(StatesGroup):
    menu = State()
    password_check = State()


class ChannelsState(StatesGroup):
    main = State()
    detail = State()
    remove_confirm = State()


class AdsState(StatesGroup):
    main = State()
    create = State()
    list = State()
    detail = State()
    rename = State()
    text = State()
    media = State()
    links = State()
    link_new = State()
    link_url = State()
    links_list = State()
    schedule = State()
    time = State()
    channels = State()
    preview = State()
    delete = State()


class SettingsState(StatesGroup):
    menu = State()
    interval = State()
