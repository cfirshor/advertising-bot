import asyncio

from aiogram.types.input_media import InputMediaAnimation, InputMediaPhoto, InputMediaVideo
from aiogram.types.message import ParseMode
from aiogram.utils.markdown import escape_md
# from croniter import croniter
import cron_descriptor
from tortoise import fields, Tortoise
from tortoise.exceptions import DoesNotExist
from tortoise.models import Model

from . import keyboards
from .tasks import send_repeatedly


class User(Model):
    id = fields.IntField(pk=True)
    chat_id = fields.IntField()
    is_authorized = fields.BooleanField(default=False)


class Channel(Model):
    id = fields.IntField(pk=True)
    chat_id = fields.IntField()
    name = fields.CharField(max_length=128)
    url = fields.CharField(max_length=2048, null=True)


class Ad(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=64, unique=True)
    enabled = fields.BooleanField(default=False)
    text = fields.CharField(max_length=4096, null=True)
    channels = fields.ManyToManyField('models.Channel', related_name='ads')
    # interval = fields.CharField(max_length=11, default='* *')
    cron_expr = fields.CharField(max_length=36, default='15 0/1 * * *')  # TODO: Migration!
    # updated_at = fields.DatetimeField(auto_now_add=True)
    
    @property
    def cron_expr_human(self):
        return cron_descriptor.get_description(self.cron_expr)

    # @property
    # def interval_cron(self):
    #     return '{} * * *'.format(self.interval)

    # async def restart_task(self):
    #     task_name = 'task_{}'.format(self.id)
    #     for task in asyncio.all_tasks():
    #         if task.get_name() == task_name:
    #             task.cancel()
    #     if self.enabled:
    #         asyncio.create_task(send_repeatedly(self), name=task_name)

    # async def send(self, bot, chat_id):
    #     links = await self.links.all()
    #     markup = keyboards.ad_buttons(links)
    #     media = await self.media.all()
    #     if media:
    #         if len(media) == 1:
    #             type_map = {
    #                 AdMedia.PHOTO: bot.send_photo, AdMedia.VIDEO: bot.send_video,
    #                 AdMedia.ANIMATION: bot.send_animation
    #             }
    #             media = media[0]
    #             await type_map[media.type](chat_id, media.file_id, caption=self.text,
    #                                                      parse_mode=ParseMode.MARKDOWN, reply_markup=markup)
    #             return
    #         else:
    #             type_map = {
    #                 AdMedia.PHOTO: InputMediaPhoto, AdMedia.VIDEO: InputMediaVideo,
    #                 AdMedia.ANIMATION: InputMediaAnimation
    #             }
    #             group = []
    #             for item in media:
    #                 class_ = type_map[item.type]
    #                 group.append(class_(item.file_id))
    #             await bot.send_media_group(chat_id, group)
    #     await bot.send_message(chat_id, self.text, parse_mode=ParseMode.MARKDOWN, reply_markup=markup)


class AdMedia(Model):
    PHOTO = 'photo'
    VIDEO = 'video'
    ANIMATION = 'animation'
    TYPES = (PHOTO, VIDEO, ANIMATION)

    id = fields.IntField(pk=True)
    ad = fields.ForeignKeyField('models.Ad', related_name='media')
    file_id = fields.CharField(max_length=256)
    type = fields.CharField(max_length=9)


class AdLink(Model):
    id = fields.IntField(pk=True)
    ad = fields.ForeignKeyField('models.Ad', related_name='links')
    order = fields.IntField()
    text = fields.CharField(max_length=45)
    url = fields.CharField(max_length=2048)

    class Meta:
        ordering = ['order']


CONFIG = {
    'connections': {'default': 'sqlite://db.sqlite3'},
    'apps': {
        'models': {
            'models': ['bot.models', 'bot.signals',
                       # 'aerich.models'
                       ],
            'default_connection': 'default'
        }
    }
}


async def init_db():
    await Tortoise.init(config=CONFIG)
    await Tortoise.generate_schemas()
