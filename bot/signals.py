import asyncio

from tortoise.signals import post_delete, post_save, pre_delete, pre_save

from .models import Ad, AdMedia, AdLink, Channel
from .tasks import send_repeatedly
from .utils import stop_task

# @pre_save(Signal)
# async def signal_pre_save(
#     sender: "Type[Signal]", instance: Signal, using_db, update_fields
# ) -> None:
#     print(sender, instance, using_db, update_fields)


# @pre_save(Ad, AdMedia, AdLink)
# async def signal_post_save(sender, instance, using_db, update_fields):
#     print('Got pre signal.')
#     print(sender, instance, update_fields)
#     print(instance.text)


@post_save(Ad)
async def signal_post_save(sender, instance, created, using_db, update_fields):
    if update_fields:
        if 'enabled' in update_fields or 'interval' in update_fields:
            task_name = str(instance.id)
            await stop_task(task_name)
            print('Stopped task of "{}"'.format(instance.name))
            if not instance.enabled:
                return
            asyncio.create_task(send_repeatedly(instance), name=task_name)
            print('Started task of "{}"'.format(instance.name))


# @pre_delete(Signal)
# async def signal_pre_delete(
#     sender: "Type[Signal]", instance: Signal, using_db: "Optional[BaseDBAsyncClient]"
# ) -> None:
#     print(sender, instance, using_db)


@post_delete(Ad)
async def signal_post_delete(sender, instance, using_db):
    # TODO: when delete added
    print('post del signal.')
    print(sender, instance, using_db)
    print(instance.id)
