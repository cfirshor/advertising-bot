import asyncio
from configparser import ConfigParser
import datetime
import logging
import math
from operator import add, sub
import re
from typing import Union
from urllib.parse import urlsplit, urlunsplit

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import ChatTypeFilter, ContentTypeFilter, Regexp, Text
from aiogram.types.chat import ChatType
from aiogram.types.message import ContentType, ParseMode
from aiogram.utils.exceptions import BadRequest
from aiogram.types.reply_keyboard import ReplyKeyboardRemove
from croniter import croniter
import cron_descriptor
from dateutil.parser import parse, ParserError
from tortoise.exceptions import DoesNotExist
import validators

from . import keyboards
from .bot import bot, dispatcher
from .models import User, Channel, Ad, AdMedia, AdLink
from .states import MainState, ChannelsState, AdsState, SettingsState
from .utils import send_ad

logger = logging.getLogger(__name__)


async def enter_channels_list(callback, state):
    await ChannelsState.main.set()
    channels = await Channel.all()
    async with state.proxy() as user_data:
        try:
            page = user_data['list_page']
        except KeyError:
            page = 1
            user_data['list_page'] = 1
    msg = '🎯  Channels\n<i>You can add new group or channel by adding bot to it.</i>'
    await callback.message.edit_text(msg, reply_markup=keyboards.channels_list(channels, page))
    await callback.answer()


async def remove_ad_preview(state, chat_id):
    async with state.proxy() as data:
        msg_ids = data.pop('ad_preview', None)
    if msg_ids:
        for msg_id in msg_ids:
            await bot.delete_message(chat_id, msg_id)


async def enter_ad_detail(ad, update: Union[types.Message, types.CallbackQuery], state):
    await AdsState.detail.set()
    is_callback = type(update) == types.CallbackQuery
    message = update.message if is_callback else update
    if ad.text or await ad.media.all().exists():
        msg_ids = await send_ad(ad, message.chat.id)
        await state.update_data({'ad_preview': msg_ids})
        msg = 'Add ad text and/or media.'
    else:
        msg = 'Edit ad'
    await message.answer(msg, reply_markup=keyboards.ad_detail(ad))
    if is_callback:
        await message.delete()
        await update.answer()


# def authorization_required(func):
#
#     async def wrapper(update, *args, **kwargs):
#         message = update.message if type(update) == types.CallbackQuery else update
#         user, created = await User.get_or_create(chat_id=message.chat.id)
#         if created or not user.is_authorized:
#             await MainState.password_check.set()
#             if type(update) == types.CallbackQuery:
#                 await message.delete()
#             await message.answer('🔐 Please enter password')
#         else:
#             return await func(update, *args, **kwargs)
#     return wrapper


@dispatcher.message_handler(ChatTypeFilter([ChatType.GROUP, ChatType.SUPERGROUP, ChatType.CHANNEL]), commands=['start'],
                            state='*')
@dispatcher.callback_query_handler(ChatTypeFilter([ChatType.GROUP, ChatType.SUPERGROUP, ChatType.CHANNEL]),
                                   state='*')  # FIXME: Can inline buttons exist in group (from forward?)?
async def group_fallback(update: Union[types.Message, types.CallbackQuery], state: FSMContext):
    # msg = 'Not available in a group.'
    msg = '👁'
    if type(update) == types.CallbackQuery:
        await update.answer(msg)
    else:
        await update.reply(msg)


text = ('<pre>'
        '╔╦╗╔═╗\n'
        ' ║ ║ ╦          version : 2.5\n'
        ' ╩ ╚═╝\n'
        '╔═╗┬─┐┌─┐    ╦═╗┌─┐─┬─\n'
        '╠═╣│ │└─┐ ── ╠═╣│ │ │\n'
        '╩ ╩┴─┘└─┘    ╩═╝└─┘ ┴'
        '</pre>\n\n'
        'Powered by:\n'
        'https://t.me/TG_Overdose')


@dispatcher.message_handler(commands=['start'], state='*')
async def on_start(message: types.Message, state: FSMContext):
    await state.reset_data()
    user, created = await User.get_or_create(chat_id=message.chat.id)
    if created or not user.is_authorized:
        await MainState.password_check.set()
        await message.answer('🔐 Please enter password')
    else:
        await MainState.menu.set()
        await message.answer(text, reply_markup=keyboards.main_menu())


@dispatcher.message_handler(state=MainState.password_check)
async def on_password_check(message: types.Message, state: FSMContext):
    config = ConfigParser()
    config.read('config.ini')
    password = config.get('main', 'password')
    if message.text.strip() == password:
        user = await User.get(chat_id=message.chat.id)
        user.is_authorized = True
        await user.save()
        await MainState.menu.set()
        await message.answer(text, reply_markup=keyboards.main_menu())
    else:
        await message.reply('🔒 Entered password is not valid. Please try again.')


@dispatcher.callback_query_handler(Regexp(r'^to_menu$'), state='*')
async def on_back_to_menu(callback: types.CallbackQuery, state: FSMContext):
    await state.reset_state()
    await MainState.menu.set()
    await callback.message.edit_text(text, reply_markup=keyboards.main_menu())
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'^channels$'), state=MainState.menu)
async def on_channels(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    # msg = data['ad_preview']
    await enter_channels_list(callback, state)


@dispatcher.my_chat_member_handler(ChatTypeFilter(chat_type=(ChatType.GROUP, ChatType.SUPERGROUP, ChatType.CHANNEL)),
                                   state='*')
async def on_add_channel(update):
    chat = update.chat
    status = update.new_chat_member.status
    if status in ('left', 'kicked'):
        try:
            channel = await Channel.get(chat_id=chat.id)
            msg = 'Bot has been removed from <b>{}</b>.'.format(channel.name)
            await channel.delete()
            for user in await User.filter(is_authorized=True):
                await update.bot.send_message(user.chat_id, msg)
        except DoesNotExist:
            pass
    elif status in ('member', 'administrator'):
        name = chat.title
        url = await chat.get_url()
        _, created = await Channel.get_or_create(chat_id=chat.id, name=name, url=url)
        if created:
            try:
                user = await User.get(chat_id=update.from_user.id)
            except DoesNotExist:
                pass
            else:
                await update.bot.send_message(user.chat_id, 'Channel <b>{}</b> has been added.'.format(name))


@dispatcher.callback_query_handler(Regexp(r'^(prev|next)$'), state=ChannelsState.main)
async def on_channels_page(callback: types.CallbackQuery, state: FSMContext):
    data = callback.data
    user_data = await state.get_data()
    page = user_data['list_page']
    channels = await Channel.all()
    last_page = math.ceil(len(channels) / keyboards.PAGE_SIZE)
    if data == 'prev':
        page = page - 1 or last_page
    else:
        page += 1
        if page > last_page:
            page = 1
    await state.update_data({'list_page': page})
    msg = '🎯  Channels\n<i>You can add new group or channel by adding bot to it.</i>'
    await callback.message.edit_text(msg, reply_markup=keyboards.channels_list(channels, page))
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'\d+'), state=ChannelsState.main)
async def on_channels_select(callback: types.CallbackQuery, state: FSMContext):
    id = int(callback.data)
    try:
        channel = await Channel.get(id=id)
    except DoesNotExist:
        await enter_channels_list(callback, state)
    else:
        await ChannelsState.detail.set()
        await state.update_data({'channel_id': channel.id})
        msg = 'Channel <b>{}</b>'.format(channel.name)
        await callback.message.edit_text(msg, reply_markup=keyboards.channel_detail(channel))
        await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'^remove$'), state=ChannelsState.detail)
async def on_channel_remove(callback: types.CallbackQuery, state: FSMContext):
    await ChannelsState.remove_confirm.set()
    await callback.message.edit_text('Are you sure?', reply_markup=keyboards.yes_no())
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'^yes$'), state=ChannelsState.remove_confirm)
async def on_channel_remove_confirm(callback: types.CallbackQuery, state: FSMContext):
    channel_id = (await state.get_data())['channel_id']
    try:
        channel = await Channel.get(id=channel_id)
    except DoesNotExist:
        pass
    else:
        await channel.delete()
    await enter_channels_list(callback, state)


@dispatcher.callback_query_handler(Regexp(r'^no$'), state=ChannelsState.remove_confirm)
async def on_channel_remove_cancel(callback: types.CallbackQuery, state: FSMContext):
    channel_id = (await state.get_data())['channel_id']
    try:
        channel = await Channel.get(id=channel_id)
    except DoesNotExist:
        await enter_channels_list(callback, state)
    else:
        await ChannelsState.detail.set()
        msg = 'Channel <b>{}</b>'.format(channel.name)
        await callback.message.edit_text(msg, reply_markup=keyboards.channel_detail(channel))
        await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'^back$'), state=ChannelsState.detail)
async def on_back_to_channels(callback: types.CallbackQuery, state: FSMContext):
    await enter_channels_list(callback, state)


@dispatcher.callback_query_handler(Regexp(r'^ads'), state=MainState.menu)
async def on_ads(callback: types.CallbackQuery, state: FSMContext):
    await AdsState.main.set()
    await callback.message.edit_text('🎙  Ads', reply_markup=keyboards.ads_menu())
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'^create$'), state=AdsState.main)
async def on_ad_create(callback: types.CallbackQuery, state: FSMContext):
    await AdsState.create.set()
    await callback.message.edit_text('Please enter ad name.', reply_markup=keyboards.back(to_menu=False))
    await callback.answer()


@dispatcher.message_handler(state=AdsState.create)
async def on_ad_created(message: types.Message, state: FSMContext):
    name = message.text
    if len(name) > 20:
        msg = '🚫 Invalid name. <i>Max length is 20 symbols.</i> Please enter ad name.'
    elif await Ad.filter(name=name).exists():
        msg = '🚫 Ad with such name already exists. Please enter another name.'
    else:
        await AdsState.detail.set()
        ad = await Ad.create(name=name)
        await state.set_data({'detail_id': ad.id})
        msg = 'Please add text and/or media, interval and channels to enable the ad.'
        await message.answer(msg, reply_markup=keyboards.ad_detail(ad))
        return
    await message.reply(msg, reply_markup=keyboards.back(False))


@dispatcher.callback_query_handler(Text(['my_ads', 'back']), state=(AdsState.main, AdsState.detail))
async def on_my_ads(callback: types.CallbackQuery, state: FSMContext):
    await remove_ad_preview(state, callback.message.chat.id)
    ads = await Ad.all()
    async with state.proxy() as data:
        try:
            page = data['list_page']
        except KeyError:
            page = 1
            data['list_page'] = page
    await AdsState.list.set()
    reply_markup = await keyboards.ads_list(ads, page)
    await callback.message.edit_text('🗂  My ads', reply_markup=reply_markup)
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'\d+'), state=AdsState.list)
async def on_my_ads_select(callback: types.CallbackQuery, state: FSMContext):
    id = int(callback.data)
    try:
        ad = await Ad.get(id=id)
    except DoesNotExist:
        ads = await Ad.all()
        await state.update_data({'list_page': 1})
        reply_markup = await keyboards.ads_list(ads)
        await callback.message.edit_text('🗂  My ads', reply_markup=reply_markup)
    else:
        await state.update_data({'detail_id': id})
        await enter_ad_detail(ad, callback, state)


@dispatcher.callback_query_handler(Text(['prev', 'next']), state=AdsState.list)
async def on_my_ads_page(callback: types.CallbackQuery, state: FSMContext):
    data = callback.data
    user_data = await state.get_data()
    page = user_data['list_page']
    ads = await Ad.all()
    last_page = math.ceil(len(ads) / keyboards.PAGE_SIZE)
    if data == 'prev':
        page = page - 1 or last_page
    else:
        page += 1
        if page > last_page:
            page = 1
    await state.update_data({'list_page': page})
    reply_markup = await keyboards.ads_list(ads, page)
    await callback.message.edit_text('Please select channels to send ad to', reply_markup=reply_markup)
    await callback.answer()


@dispatcher.callback_query_handler(Text('rename'), state=AdsState.detail)
async def on_ad_rename(callback: types.CallbackQuery, state: FSMContext):
    await AdsState.rename.set()
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    await remove_ad_preview(state, callback.message.chat.id)
    msg = ('Name: <b>{}</b>\n'
           'Please enter a new name').format(ad.name)
    await callback.message.edit_text(msg, reply_markup=keyboards.back(False))
    await callback.answer()


@dispatcher.message_handler(state=AdsState.rename)
async def on_ad_new_name(message: types.Message, state: FSMContext):
    name = message.text
    data = await state.get_data()
    id = data['detail_id']
    if len(name) > 20:
        msg = '🚫 Invalid name. <i>Max length is 20 symbols.</i>\n' \
              'Please enter new name.'
    elif await Ad.exclude(id=id).filter(name=name).exists():
        msg = '🚫 Another ad with such name already exists.\n' \
              'Please enter new name.'
    else:
        ad = await Ad.get(id=id)
        ad.name = name
        await ad.save()
        await enter_ad_detail(ad, message, state)
        return
    await message.answer(msg, reply_markup=keyboards.back(False))


@dispatcher.callback_query_handler(Text('text'), state=AdsState.detail)
async def on_ad_change_text(callback: types.CallbackQuery, state: FSMContext):
    await remove_ad_preview(state, callback.message.chat.id)
    await AdsState.text.set()
    await callback.message.edit_text('Please enter new text.', reply_markup=keyboards.back(False))
    await callback.answer()


@dispatcher.message_handler(state=AdsState.text)
async def on_ad_text_set(message: types.Message, state: FSMContext):
    text = message.text
    if len(text) > 1024:
        msg_text = '🚫 Cannot be longer than 1,024 characters. \n\n<b>Text count:</b> {}'.format(str(len(text)))
        await message.reply(msg_text, reply_markup=keyboards.back(False))
    else:
        data = await state.get_data()
        ad = await Ad.get(id=data['detail_id'])
        if ad.text != text:
            ad.text = text
            await ad.save(update_fields=['text'])
        # await ad.restart_task()
        await enter_ad_detail(ad, message, state)


@dispatcher.callback_query_handler(Text('media'), state=AdsState.detail)
async def on_ad_media(callback: types.CallbackQuery, state: FSMContext):
    await state.update_data({'media': []})
    await remove_ad_preview(state, callback.message.chat.id)
    await callback.message.delete()
    await AdsState.media.set()
    msg = ('Please upload images and/or video.\n'
           '<i>Animations cannot be used in media groups and will be skipped if multiple files sent.</i>')
    await callback.message.answer('Please upload images and/or video.', reply_markup=keyboards.done_reply_keyboard())
    await callback.answer()


@dispatcher.message_handler(content_types=(ContentType.PHOTO, ContentType.VIDEO, ContentType.ANIMATION),
                            state=AdsState.media)
async def on_ad_media_upload(message: types.Message, state: FSMContext):
    # TODO: add logic for removing caption text over 1024 chars, if user add an image(for now text allowed only 1024)
    for type in ('photo', 'video', 'animation'):
        file = getattr(message, type)
        if file:
            if isinstance(file, list):
                file = file[-1]
            # if type == 'animation':
            #     type = 'video'
            async with state.proxy() as data:
                data['media'].append((file.file_id, type))
            break


# TODO: Update task on any edit
@dispatcher.message_handler(Text('✔ Done'), state=AdsState.media)
async def on_ad_media_done(message: types.Message, state: FSMContext):
    await state.reset_state(with_data=False)
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    await ad.media.all().delete()
    media = data['media']
    if media:
        if len(media) > 1:
            media = filter(lambda x: x[1] != 'animation', media)
        for file_id, file_type in media:
            await AdMedia.create(ad=ad, file_id=file_id, type=file_type)
    # await ad.restart_task()
    # TODO: Make sure keyboard is removed
    # await message.answer(message.text, reply_markup=ReplyKeyboardRemove())
    await enter_ad_detail(ad, message, state)


@dispatcher.message_handler(Text('❌ Cancel'), state=AdsState.media)
async def on_ad_media_cancel(message: types.Message, state: FSMContext):
    # TODO: check tortoise .get caching?
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    await enter_ad_detail(ad, message, state)


@dispatcher.callback_query_handler(Text('links'), state=AdsState.detail)
async def on_ad_links(callback: types.CallbackQuery, state: FSMContext):
    await remove_ad_preview(state, callback.message.chat.id)
    await AdsState.links.set()
    await callback.message.edit_text('🔗 Link buttons', reply_markup=keyboards.link_buttons())
    await callback.answer()


@dispatcher.callback_query_handler(Text('new'), state=AdsState.links)
async def on_ad_link_new(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    await AdsState.link_new.set()
    if await ad.links.all().count() == 20:
        await callback.answer('Max number of buttons is reached (20)')
    else:
        await callback.message.answer('Please enter link text', reply_markup=keyboards.back(to_menu=False))
        await callback.answer()


@dispatcher.message_handler(state=AdsState.link_new)
async def on_ad_link_text(message: types.Message, state: FSMContext):
    text = message.text
    if len(text) > 45:
        msg = '🚫 Maximum length is 45 symbols. Please enter shorter text.'
        await message.reply(msg, reply_markup=keyboards.back(False))
    else:
        await AdsState.link_url.set()
        await state.update_data({'ad_link': {'text': text}})
        await message.answer('Please enter link URL.', reply_markup=keyboards.back(False))


@dispatcher.message_handler(state=AdsState.link_url)
@dispatcher.callback_query_handler(Text('back'), state=AdsState.link_url)
async def on_ad_link_url(update, state: FSMContext):
    if type(update) == types.CallbackQuery:
        await AdsState.link_new.set()
        await update.message.answer('Please enter link text.', reply_markup=keyboards.back(to_menu=False))
        await update.answer()
    else:
        # TODO: check if TG username and swap the @ to "http://t.me/"
        url = update.text
        if not urlsplit(url).scheme:
            url = 'https://' + url
        if validators.url(url):
            await AdsState.links.set()
            data = await state.get_data()
            ad = await Ad.get(id=data['detail_id'])
            num_links = await ad.links.all().count()
            text = data['ad_link']['text']
            await AdLink.create(ad=ad, order=num_links, text=text, url=url)
            # await ad.restart_task()
            await update.answer('Link has been created.')
            await update.answer('🔗 Link buttons', reply_markup=keyboards.link_buttons())
        else:
            await update.reply('🚫 Invalid URL.\nPlease try again.', reply_markup=keyboards.back(to_menu=False))


@dispatcher.callback_query_handler(Text('list'), state=AdsState.links)
async def on_ad_links_list(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    links = await ad.links.all()
    await AdsState.links_list.set()
    await callback.message.answer('🎹 Buttons', reply_markup=keyboards.links_list(links))
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'(up_\d+|down_\d+)'), state=AdsState.links_list)
async def on_ad_link_order(callback: types.CallbackQuery, state: FSMContext):
    action, link_id = callback.data.split('_')
    link = await AdLink.get(id=link_id)
    ad = await link.ad
    # num_links = await ad.links.all().count()
    num_links = await ad.links.all().count()
    if num_links > 1:
        op_map = {'up': sub, 'down': add}
        op = op_map[action]
        order = op(link.order, 1)
        if order < 0:
            order = num_links - 1
        elif order == num_links:
            order = 0
        link_sub = await AdLink.get(ad=ad, order=order)
        link_sub.order = link.order
        link.order = order
        await link_sub.save()
        await link.save()
        # await ad.restart_task()
        links = await ad.links.all()
        await callback.message.edit_reply_markup(keyboards.links_list(links))
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'delete_\d+'), state=AdsState.links_list)
async def on_ad_link_delete(callback: types.CallbackQuery, state: FSMContext):
    link_id = callback.data.split('_')[1]
    link = await AdLink.get(id=link_id)
    ad = await link.ad
    order = link.order
    await link.delete()
    for link in await ad.links.filter(order__gt=order):
        link.order -= 1
        await link.save()
    # await ad.restart_task()
    links = await ad.links.all()
    await callback.message.edit_reply_markup(keyboards.links_list(links))
    await callback.answer()


@dispatcher.callback_query_handler(Text('back'), state=(AdsState.link_new, AdsState.links_list))
async def on_back_to_ad_links(callback: types.CallbackQuery, state: FSMContext):
    await AdsState.links.set()
    await callback.message.edit_text('🔗 Link buttons', reply_markup=keyboards.link_buttons())
    await callback.answer()


@dispatcher.callback_query_handler(Text('schedule'), state=AdsState.detail)
async def on_ad_schedule(callback: types.CallbackQuery, state: FSMContext):
    await remove_ad_preview(state, callback.message.chat.id)
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    msg = ('Schedule: <code>{}</code>\n'
           '"{}."\n\n'
           'Please enter cron expression defining the schedule.').format(ad.cron_expr, ad.cron_expr_human)
    await AdsState.schedule.set()
    await callback.message.edit_text(msg, reply_markup=keyboards.back(to_menu=False))
    await callback.answer()


@dispatcher.message_handler(state=AdsState.schedule)
@dispatcher.callback_query_handler(Text('correct'), state=AdsState.schedule)
async def on_ad_schedule_enter(update: Union[types.Message, types.CallbackQuery], state: FSMContext):
    if type(update) == types.CallbackQuery:
        data = await state.get_data()
        val = data['schedule']
        ad = await Ad.get(id=data['detail_id'])
        if ad.cron_expr != val:
            ad.cron_expr = val
            await ad.save(update_fields=['cron_expr'])
        # await ad.restart_task()
        await enter_ad_detail(ad, update, state)
    else:
        val = update.text.strip()
        if not croniter.is_valid(val):
            msg = '🚫 Please enter valid cron expression.' \
                  '\nCron examples: <a href="https://crontab.guru/examples.html/">Cronmaker</a>'
            await update.reply(msg, reply_markup=keyboards.back(False))
        else:
            await state.update_data({'schedule': val})
            msg = 'Entered schedule: "{}."\nRe-enter expression if not correct.'.format(
                cron_descriptor.get_description(val))
            await update.answer(msg, reply_markup=keyboards.confirm())


# @dispatcher.callback_query_handler(Regexp('^change_time$'), state=AdsState.detail)
# async def on_ad_change_time(callback: types.CallbackQuery, state: FSMContext):
#     await AdsState.time.set()
#     ad_id = (await state.get_data())['detail_id']
#     ad = await Ad.get(id=ad_id)
#     msg = ('Please enter time or date and time, when to start sending ad.\n'
#            'For example 12:30 <b>or</b> 21-12-2021 12:30\n\n'
#            '<b>Bot time: {}</b>').format(datetime.datetime.now().strftime('%d-%m-%Y %H:%M'))
#     if ad.send_at:
#         msg = 'Start time: {}\n\n{}'.format(ad.send_at.strftime('%d-%m-%Y %H:%M'), msg)
#     await callback.message.edit_text(msg, reply_markup=keyboards.back(to_menu=False))
#     await callback.answer()
#
#
# @dispatcher.message_handler(ContentTypeFilter(ContentType.TEXT), state=AdsState.time)
# async def on_ad_enter_time(message: types.Message, state: FSMContext):
#     try:
#         start_time = parse(message.text.strip())
#     except (ParserError, OverflowError):
#         await message.reply('🚫 Invalid format.', reply_markup=keyboards.back(to_menu=False))
#     else:
#         now = datetime.datetime.now()
#         if start_time < now:
#             msg = 'Start time should be greater than current time\n' \
#                   '<i>Bot time: {}</i>'.format(now.strftime('%d-%m-%Y %H:%M'))
#             await message.reply(msg, reply_markup=keyboards.back(to_menu=False))
#         else:
#             await AdsState.detail.set()
#             ad_id = (await state.get_data())['detail_id']
#             ad = await Ad.get(id=ad_id)
#             ad.send_at = start_time
#             await ad.save()
#             await ad.restart_task()
#             reply_markup = keyboards.ad_detail(ad.enabled)
#             await message.answer(ad.detail_text(), reply_markup=reply_markup)


# @dispatcher.callback_query_handler(Regexp('^now$'), state=AdsState.change_time)
# async def on_ad_time_set_now(callback: types.CallbackData, state: FSMContext):
#     await AdsState.detail.set()
#     ad_id = (await state.get_data())['detail_id']
#     ad = await AdsState.get(id=ad_id)
#     if ad.enabled:
#         ad.send_at = datetime.datetime.now() + datetime.timedelta(seconds=15)
#         await ad.save()
#         await ad.restart_task()
#     else:
#         start_time = None
#     ad.send_at = start_time
#     await ad.save()
#     await ad.restart_task(callback.bot)
#     reply_markup = keyboards.ad_detail(ad.enabled)
#     await callback.message.edit_text(ad.detail_text(), reply_markup=reply_markup)
#     await callback.answer()


@dispatcher.callback_query_handler(Text('channels'), state=AdsState.detail)
async def on_ad_channels(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    channels = await Channel.all()
    selected = await ad.channels.all().values_list('id', flat=True)
    selected = [str(val) for val in selected]
    await state.update_data({'channels_page': 1, 'selected': selected})
    msg = 'Please select channels to send ad to'
    reply_markup = keyboards.channels_select_multiple(channels, selected)
    await AdsState.channels.set()
    await remove_ad_preview(state, callback.message.chat.id)
    await callback.message.edit_text(msg, reply_markup=reply_markup)
    await callback.answer()


@dispatcher.callback_query_handler(Regexp(r'(\d+|all)'), state=AdsState.channels)
async def on_ad_channels_select(callback: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as user_data:
        value = callback.data
        selected = user_data['selected']
        if value == 'all':
            if selected:
                selected = []
            else:
                selected = [str(val) for val in await Channel.all().values_list('id', flat=True)]
        else:
            if value in selected:
                selected.remove(value)
            else:
                selected.append(value)
        user_data['selected'] = selected
        page = user_data['channels_page']
    channels = await Channel.all()
    reply_markup = keyboards.channels_select_multiple(channels, selected, page)
    await callback.message.edit_reply_markup(reply_markup=reply_markup)
    await callback.answer()


@dispatcher.callback_query_handler(Text(['prev', 'next']), state=AdsState.channels)
async def on_ad_channels_page(callback: types.CallbackQuery, state: FSMContext):
    val = callback.data
    data = await state.get_data()
    page = data['channels_page']
    channels = await Channel.all()
    last_page = math.ceil(len(channels) / keyboards.PAGE_SIZE)
    if val == 'prev':
        page = page - 1 or last_page
    else:
        page += 1
        if page > last_page:
            page = 1
    await state.update_data({'channels_page': page})
    selected = data['selected']
    reply_markup = keyboards.channels_select_multiple(channels, selected, page)
    await callback.message.edit_text('Please select channels to send ad to', reply_markup=reply_markup)
    await callback.answer()


@dispatcher.callback_query_handler(Text('save'), state=AdsState.channels)
async def on_ad_channels_done(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    ids = [int(val) for val in data['selected']]
    ad = await Ad.get(id=data['detail_id'])
    await ad.channels.clear()
    if ids:
        channels = await Channel.filter(id__in=ids)
        await ad.channels.add(*channels)
    # await ad.restart_task()
    await enter_ad_detail(ad, callback, state)


@dispatcher.callback_query_handler(Text('status'), state=AdsState.detail)
async def on_ad_status_toggle(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    if not ad.enabled:
        has_media = await ad.media.all().exists()
        has_channels = await ad.channels.all().exists()
        if not (ad.text or has_media) or not has_channels:
            await callback.answer('Please add text and/or media and specify channels.')
            return
    ad.enabled = not ad.enabled
    await ad.save(update_fields=['enabled'])
    # await ad.restart_task()
    await callback.message.edit_reply_markup(reply_markup=keyboards.ad_detail(ad))
    await callback.answer()


@dispatcher.callback_query_handler(Text('delete'), state=AdsState.detail)
async def on_ad_delete(callback: types.CallbackQuery, state: FSMContext):
    await AdsState.delete.set()
    await callback.message.edit_text('Are you sure?', reply_markup=keyboards.yes_no())
    await callback.answer()


@dispatcher.callback_query_handler(Text(['yes', 'no']), state=AdsState.delete)
async def on_ad_delete_confirm(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    resp = callback.data
    if resp == 'yes':
        await ad.delete()
        ads = await Ad.all()
        page = 1
        await state.update_data({'list_page': page})
        await AdsState.list.set()
        reply_markup = await keyboards.ads_list(ads, page)
        await callback.message.edit_text('🗂  My ads', reply_markup=reply_markup)
        await callback.answer()
    else:
        await enter_ad_detail(ad, callback, state)


@dispatcher.callback_query_handler(Text('back'), state=(AdsState.rename, AdsState.text, AdsState.media, AdsState.links,
                                                        AdsState.schedule, AdsState.time, AdsState.channels))
async def on_back_to_ad_detail(callback: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    ad = await Ad.get(id=data['detail_id'])
    await enter_ad_detail(ad, callback, state)


@dispatcher.callback_query_handler(Text('back'), state=(AdsState.list, AdsState.create))
async def on_back_to_ads_menu(callback: types.CallbackQuery, state: FSMContext):
    await state.reset_state()
    await AdsState.main.set()
    await callback.message.edit_text('🎙 Ads', reply_markup=keyboards.ads_menu())
    await callback.answer()


@dispatcher.callback_query_handler(Text('settings'), state=MainState.menu)
async def on_settings(callback: types.CallbackQuery, state: FSMContext):
    await SettingsState.menu.set()
    await callback.message.edit_text('⚙ Settings', reply_markup=keyboards.settings())
    await callback.answer()


@dispatcher.callback_query_handler(Text('interval'), state=SettingsState.menu)
async def on_settings_interval(callback: types.CallbackQuery, state: FSMContext):
    await SettingsState.interval.set()


@dispatcher.callback_query_handler(Regexp(r'^blank$'), state='*')
async def on_do_nothing(callback: types.CallbackQuery, state: FSMContext):
    await callback.answer()

# TODO: When editing groups, check page, so added/deleted groups won't mess up anything
